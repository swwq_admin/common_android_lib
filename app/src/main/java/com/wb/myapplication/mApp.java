package com.wb.myapplication;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.alibaba.android.arouter.launcher.ARouter;
import com.github.anzewei.parallaxbacklayout.ParallaxHelper;
import com.tencent.mmkv.MMKV;
import com.wb.lib_image_loadcal.ImageManager;
import com.wb.lib_utils.AppUtils;
import com.wb.lib_utils.utils.log.LogUtils;

import me.jessyan.autosize.AutoSizeConfig;

public class mApp extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //////
        MultiDex.install(base);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        if (AppUtils.isDebug()) {
            ARouter.openLog();     // 打印日志
            ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        }
        AutoSizeConfig.getInstance().setExcludeFontScale(true);
        ARouter.init(this);
        MMKV.initialize(this);
        ImageManager.getInstance().init(this.getApplicationContext());
        AutoSizeConfig.getInstance().setLog(false).setUseDeviceSize(false);
        AppUtils.getApp().registerActivityLifecycleCallbacks(ParallaxHelper.getInstance());
    }
}
