package com.wb.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lib_common.base.BaseFragment;

public class Test3Fragment extends BaseFragment {
    @Override
    public int initLayoutResId() {
        return R.layout.main1;
    }
    @Override
    public void initView(Bundle savedInstanceState) {
        TextView textView=this.findViewById(R.id.tzBt);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFragment(new Test1Fragment());
            }
        });
    }

    @Override
    public void initViewData() {

    }

    @Override
    public void initObservable() {

    }
}
