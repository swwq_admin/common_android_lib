package com.wb.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lib_common.base.BaseFragment;
import com.wb.lib_utils.utils.ToastUtils;

public class TestFragment extends BaseFragment {
    @Override
    public int initLayoutResId() {
        return R.layout.main1;
    }
    @Override
    public void initView(Bundle savedInstanceState) {
        TextView textView=this.findViewById(R.id.tzBt);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.normal("dadada");
//                startFragment(new Test2Fragment());
            }
        });
    }

    @Override
    public void initViewData() {

    }

    @Override
    public void initObservable() {

    }
}
