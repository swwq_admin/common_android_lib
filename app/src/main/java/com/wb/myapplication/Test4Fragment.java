package com.wb.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lib_common.base.BaseFragment;

public class Test4Fragment extends BaseFragment {
    @Override
    public int initLayoutResId() {
        return R.layout.main1;
    }
    @Override
    public void initView(Bundle savedInstanceState) {
    }

    @Override
    public void initViewData() {

    }

    @Override
    public void initObservable() {

    }
}
