package com.wb.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lib_common.base.BaseFragment;
import com.example.lib_common.config.BaseMsgType;
import com.wb.lib_arch.common.Bus;
import com.wb.lib_arch.common.MsgEvent;
import com.wb.lib_utils.utils.log.LogUtils;

public class Test1Fragment extends BaseFragment {
    @Override
    public int initLayoutResId() {
        return R.layout.main2;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        TextView account_log=this.findViewById(R.id.account_log3);
        account_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.e("看看-----");
                Bus.post(new MsgEvent(BaseMsgType.ACCOUNT_OUT_AUTO));
            }
        });
    }
}
