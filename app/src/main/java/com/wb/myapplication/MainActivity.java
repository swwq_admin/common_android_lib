package com.wb.myapplication;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lib_common.base.ContainerActivity;
import com.example.lib_common.config.BaseMsgType;
import com.wb.lib_arch.annotations.BindRes;
import com.wb.lib_arch.common.Bus;
import com.wb.lib_arch.common.MsgEvent;
import com.wb.lib_utils.utils.log.LogUtils;
@BindRes(isMain = true)
public class MainActivity extends ContainerActivity {
    @Override
    public Fragment initBaseFragment() {
        return new TestFragment();
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        Bus.observe(this, new Observer<MsgEvent>() {
            @Override
            public void onChanged(@Nullable MsgEvent msgEvent) {
                cleanAllActivity(true);
            }
        });
    }

    @Override
    public boolean isMain() {
        return true;
    }
}
