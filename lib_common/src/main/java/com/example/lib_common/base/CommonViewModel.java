package com.example.lib_common.base;


import com.example.lib_common.bean.MsgData;
import com.wb.lib_network.BaseViewModel;
import com.wb.lib_network.SingleLiveEvent;

public class CommonViewModel extends BaseViewModel {
    public final SingleLiveEvent<MsgData> msgEvent = new SingleLiveEvent<>();
}
