package com.wb.lib_weiget.status;

public interface StatusViewConvertListener {
    void onConvert(ViewHolder viewHolder);
}